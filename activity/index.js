


//ACTIVITY

console.log("Hello World");

// create multiple variables
let firstName = "John";
console.log(`First Name: ${firstName}`);

let lastName = "Smith";
console.log(`Last Name: ${lastName}`);

let age = "30";
console.log(`Age: ${age}`);

let hobbies = "Hobbies:"
console.log(hobbies);

let arrayHobbies = ["Biking", "Mountain Climbing", "Swimming"]
console.log(arrayHobbies);

let workAddress = "Work Address:"
console.log(workAddress);

let objectWorkAddress = {
	houseNumber: "32",
	Street: "Washington",
	city: "Lincoln",
	State: "Nebraska"
}
console.log(objectWorkAddress);

// Create a function named printUserInfo
let printUserInfo = function( firstName, lastName, age) {
	console.log(`${firstName} ${lastName} is ${age} years of age.`)

	console.log(arrayHobbies)
	console.log(objectWorkAddress)
}

printUserInfo(firstName, lastName, age);

// Create another function named returnFunction that will return a value and store it in a variable.
let returnFunction = function(booleanMarried) {
	return `isMarried is ${booleanMarried}`
}
let isMarried = returnFunction(true);
console.log(isMarried);



// STRETCH GOAL
// 1
function addition(a,b) {
	console.log(a+b)
}

addition(5,2);

// 2
function substraction(a,b) {
	console.log(a-b)
}

substraction(3,2);

// 3
function multiplication(a,b) {
	console.log(a*b)
}

multiplication(6,2);