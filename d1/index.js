

// Statements, Syntax and Comments

console.log("hello world");

// Note: Convention: Breaking up long statements into multiple lines is common practice.
/*
Multiple Comments
*/


// Declare variables:
// Syntax: let/const/var varaibleName;

const myVariable = "Hello again";
console.log(myVariable);

/*
Guides in writing variables:
1. We can use 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
2. Variable names should start with lowercase character, or use  camelcase for multiple words.
*/



//Best practices in naming variables:
//1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains. 



let firstNames = "Jane"

//2. It is better to start with lower case letter. Beacuse there are  several keywords in JS that start in capital letter.

//3. Do not add spaces to your variables names. Use camelcase words, or underscore.
	
	let product_description = "sample"


//Declaring and initializing variables
// Initializing variable-the instance when a variable is given it's initial/starting value
let producName = "desktop computer";
console.log(producName)

let producPrice = 18999;
console.log(producPrice)

const interest = 3.539;

// Reassigning variable values
producName = "laptop";
console.log(producName)


// let variable can be reassigned with another value;
// let variable cannot be re-declared within its scope;

let friend = "kate"
friend = "john"

const pi = 3.14

// Reassigning variables vs initializing variables
// let - we can declare a variable first

let supplier;
// Initialization is done after the variable has been declared

// Let's invoke/call the function that we declared


supplier = "john smith tradings";
console.log(supplier);

// reassign it's initial value
supplier = "Zuitt Store;"
console.log(supplier);

// const variable are variables with constant data.  Therefore we should  not re-declare, re-assign or even declare a const variable without initialization.

// var. vs let/const

// var - is also used in declaring a variable. var is an ECMA script (ES1) feature ESI (JS 1997).

// what makes let/const different from var?
// There are issues when we used var in declaring variables, one of these is hoisting.
// Hoisting is JS default behaviour of moving declarations to the top.

a = 5;
console.log(a)
var a;

// Scope essentially means where these variable are available to use.
// A block is a chunk of code bounded by {}. A block  lives in curly braces. Anything within curly baraces is block.
// So a variable declared in a block with let/const is only available

const outerVariable = "hello";

// Let's invoke/call the function that we declared

// block/local scope = {}

{
	const innerVariable = "hello again"
	console.log(outerVariable);
}

// console.log(outerVariable);
// console.log(innerVariable);


// example
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);




// DATA TYPES

// STRINGS
let country = 'Philippines'
let province = "Metro Manila"
console.log(typeof country);
console.log(typeof province);

// Concatenating Strings
let fullAddress = province + ', ' + country;
console.log(fullAddress)

let greeting = 'I live in the ' + country
console.log(greeting);

// Template literals is the updated version of concatenation
// backticks 'dvdv'
// expression ${}
console.log(`I live in the ${province}, ${country}`);

// Escape character(\) in strings in combination  with other character can produce different effects
// "\n" refers to creating a new line in between text

let	mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

// \'

let message = 'john\'s employees went home early';
console.log(message)

message = "Jonh's employees went home early"
console.log(message)

console.log(

	`Metro Manila

	"hello"
	'hello'

	Philippines ${province}`

	)


// NUMBERS
// Integers/Whole Numbers
let headCount = 23
console.log(typeof headCount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(typeof grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(typeof planetDistance);

// Combining variable with number data type and string
console.log("John's grade last quarter is "+ grade);
console.log(`john's grade last quarter is ${grade}`);


let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;

let numString1 = "5";
let numString2 = "6";

console.log(numString1 + numString2) //"56" strings were concatenated
console.log(num1 + num2) //11 math operation

// Type coercion/force coercion
console.log(numString1 + num1)//"55" concatenated

// Mathematical Operations (+, -, *, /, %(modulo-remainder))


// Modulo
console.log(num1 & num2); // (5/6) = remainder 5
console.log(num2 & num1); // (6/5) = remainder 1




// BOOLEAN
// Boolean values are normally used to store values relating to the state of certain things.
// This will be useful in further discussion about creating logic to make our application respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

// OBJECTS

// Arrays area special kind of data that is used to store multiple values. I can store different data types but is normally used  to store similar data types

// similar data types
// syntax: let/const arratName = [elementA, elementB, etc];


let grades = [98.7,92.1,94.6];
console.log(grades);
console.log(typeof grades);

// different data types
// storing diff data types inside an array in not recommended becuase it will not make sense to join the context of programmin.

let details = ["John", "Smith", 32, true]
console.log(details);

// indexing
console.log(grades[1], grades[0]);

// Reassigning array elements
// let anime = ['one piece', 'one punch', 'aot'];
// console.log(anime);
// anime[0] = 'kimetsu no yaiba';
// console.log(anime);

// using const
// Reassigning array elements
const anime = ['one piece', 'one punch', 'aot'];
console.log(anime);
anime[0] = 'kimetsu no yaiba';
console.log(anime);

// anime = ['kimetsu no yaiba', 'charlotte', 'solo loveling', 'buco no hero'];
// console.log(anime);


// the keyword const is a little misleading. It does not  a constant value. It defines a constant to a value.

/*
Beacuase of this you can not:
	Reassign a constant value, constant array, constant object

BUT YOU CAN:
	change the elements of constant array
	change the properties of constant object

*/


// OBJECTS
// syntax
/*
	let/const objectName = {propertyA: value, propertyB: value}

	gives an individual piece of informatio and it is called a property of the object
*/

let objectGrades = {
	firstQuarter: 98.7,
	secondQuarter: 92.1,
	thirdQuarter: 90.2,
	fourthQuarter: 94.6
};

let person = {
	fullName: 'Juan dela cruz',
	age: 35,
	isMarried: false,
	contact: ['09273227382', '9894 8989'],
	address: {
		houseNumber:'345',
		city: 'Manila'
	}
}

console.log(person);

// dot notation
console.log(person.fullName);
console.log(person.age);

console.log(person.contact[0]);
console.log(person.address.city);



//MINI ACTIVITY
 // 1
 let firstName = "Magic";
 let lastName = "Gapud";
 console.log(`${firstName}, ${lastName}`);


 // 2
 let sentence = " a student of Zuitt";
 console.log(firstName, lastName, sentence )

 // 3
 let favoriteFood = ["Ginataan", "Sinigang","Adobo"]
 console.log(favoriteFood)

 // 4
 let objectData = {
 	firstName: "Magic",
 	lastName: "Gapud",
 	isDeveloper: true,
 	hasPortfolio: true,
 	age: 28
 }
console.log(objectData)



// NULL
// It is used to intentionally express the absence of a value in a variable declaration/initialization
let girlfriend = null;
console.log(girlfriend)

let myNUmber = 0;
let myString ='';
console.log(myNUmber);
console.log(myString);
// Using null compared to a 0 value and an empty string is much better for readability purposes.

// UNDEFINED
// It represent the state of a variable that has been declared but without an assigned value.
let sampleVariable;
console.log(sampleVariable); //undefined

// One clear difference between undefined and null is that for undefined, a variable was created but was not porvided a value.
// null means that avariable was created and was assigned a value that does not hold any value/amount.

// FUNCTION
// in JS, it is a line/s/block of codes that tell our application to perform a certain task when called/invoked.

// FUNCTION DECLARATION
	// defines a function with the function and specified parameters.
		/*
			Syntax:
				function functionName() {	
					code block/statements. the block of code will be  executed once the function has been run/called/invoked.
				}

				function keyword => defined a js function
				function name => camelCase to name our function
				function block/statements => the statement which comprise the body of the function. This is where the code to be executed.
		*/

			// function printName() {
			// 	console.log("My name is John");
			// };



// FUNCTION EXPRESSION
	// a function expression can be stored in a variable

	// Anonymous function - a function without a name.
	let variableFunction = function() {
		console.log("Hello World")
	}

	let functionExpression = function func_name() {
		console.log("hello!")
	}

// FUNCTION INVOCATION
	// The code inside a function is executed when the function is called /invoked.

// Let's invoke/call the function that we declared

// printName();

function declaredFunction() {
	console.log("Hello World")
}

declaredFunction();

// How about the function expression?
// They are always invoked/called using the variable name

variableFunction();

// Function scoping

// JS scope
// global and local scope


let faveCharacter = "Nezuko-chan"

function myFunction() {
	let nickName = "Jane"; //function scope
	console.log(nickName);
}

// console.log(faveCharacter)
myFunction();
// console.log(nickName); //not defined

// Variables defined inside a function are not accessible (visible) from outside the function
// var/let/const are quite similar when declared inside a function.
// Variable declared globally (outside of the function) have a global scope or it can access anywhere.



function showSum() {
	console.log(6 + 6)
}

showSum();

// PARAMETERS AND ARGUMENTS
// "name" is called a parameter
// parameter=> acts as a named variable/container that exist  ONLY inside of the function. Usually parameter is the placeholder of an actual value
let name = "Jane Smith";


function printName(pikachu) {
	console.log(`My name is ${pikachu}`);
};


// argument is the actual value/data that we passed to our function
// We can also use a variabe as an argument
printName(name);


// Multiple Parameters and Arguments

function displayFullname(fName, lName, age) {
	console.log(`${fName} ${lName} is ${age}`)
}

displayFullname("Judy", "Medalla", "jane", 67)
// Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.
// Providing more arguments will not affect our function.

// return keyword
	// The "return" statement allows the output of a function to be passed to the block of code that invoked the function
	// any line/block of code that comes after the return statement is ignored because it ends the function execution

	// return keyword is used so that a function may return a value.
	// after returning the value, the next statement will stop  its  process

	function createFullName(firstName, middleName, lastName) {
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's value/result has been returned")
	}

	// result of this function(return) can be saved into a variable
	let fullName1 = createFullName("Tom", "Cruise", "Mapother");
	console.log(fullName1);

	// The result of a function of a function without a return keyword will not save the result in a variable.

	let fullName2 = displayFullname("Willian", "Bradly", "Pitt");
	console.log(fullName2);


	let fullName3 = createFullName("Jeffrey", "John", "Smith");
	console.log(fullName3);



	// MINI ACTIVITY

	// Create a function
		/*function divisionOperation(firstNumber, secondNumber) {
			return firstNumber/secondNumber
		}


		let quotient = divisionOperation(
			prompt("enter your first number"),
			prompt("enter your second number")
			)

		console.log(`The result of the division is: ${quotient}`)*/

		
		
		// Function as an argument

		function argumentFunction() {
			console.log("This function was passed as an argument before the message was printed")
		}

		function invokeFunction(pikachu) {
			pikachu();
		}

		invokeFunction(argumentFunction)